﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour
{
    Animator animController;

    void Start()
    {
        animController = GetComponent<Animator>();
        animController.SetBool("gameOver", false);
        animController.SetBool("IsGameWon", false);
    }
    void Update()
    {
        if (transform.parent.GetComponent<CameraRig>().characterController.gameOver)
        {
            animController.SetBool("gameOver", true);
        }

        if (transform.parent.GetComponent<CameraRig>().characterController.gameWon)
        {
            animController.SetBool("IsGameWon", true);
        }
    }

    /*Vector3 startPos;
    Quaternion startRot;
    bool lookRotation;
    bool lerpOneFinished;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        startRot = transform.rotation;
        lookRotation = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.parent.GetComponent<CameraRig>().characterController.gameOver)
        {
            //transform.Translate(new Vector3(Mathf.Lerp(transform.position.x, 0, 0.5f), transform.position.y, transform.position.z));
            //transform.Translate(new Vector3(Mathf.Lerp(transform.position.x, -6.44000006f, 0.5f), transform.position.y, Mathf.Lerp(transform.position.z, -8.97999954f, 0.5f)));

            if(lerpOneFinished == false)
            {
                StartCoroutine(LerpOne());
                //StartCoroutine(LerpTwo());
                //lerpOneFinished = false;
            }
        }

        if(lookRotation == true)
        {
            //Quaternion.LookRotation(transform.parent.transform.position);
            transform.LookAt(transform.parent.transform.position, Vector3.up);
            //StartCoroutine(LookAt());
            lerpOneFinished = true;
            StartCoroutine(LerpTwo());
        }
        else
        {
            transform.rotation = startRot;
        }
    }

    IEnumerator LerpOne()
    {
        float duration = 3.0f;
        for (float t = 0.0f; t < duration; t += Time.deltaTime)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0, transform.localPosition.y, transform.localPosition.z), t / duration);
            yield return null;
        }
        lookRotation = true;
    }

    IEnumerator LookAt()
    {
        float duration = 3.0f;
        for (float t = 0.0f; t < duration; t += Time.deltaTime)
        {
            //transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0, transform.localPosition.y, transform.localPosition.z), t / duration);
            transform.LookAt(Vector3.Lerp(transform.position, transform.parent.transform.position, t / duration), Vector3.up);
            yield return null;
        }
    }

    IEnumerator LerpTwo()
    {
        float duration = 6.0f;
        for (float t = 0.0f; t < duration; t += Time.deltaTime)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(-6.44000006f, transform.localPosition.y, -8.97999954f), t / duration);
            yield return null;
        }
    }*/
}
