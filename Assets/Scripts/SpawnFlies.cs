﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFlies : MonoBehaviour
{
    [SerializeField] GameObject flyObject;
    [SerializeField] int maxSpawn;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i <= maxSpawn; i++)
        {
            Vector3 spawnPos = new Vector3(Random.Range(transform.position.x - transform.localScale.x / 2, transform.position.x + transform.localScale.x / 2), Random.Range(transform.position.y - transform.localScale.y / 2, transform.position.y + transform.localScale.y / 2), Random.Range(transform.position.z - transform.localScale.z / 2, transform.position.z + transform.localScale.z / 2));
            Instantiate(flyObject, spawnPos, transform.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
