﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{

    // Start is called before the first frame update
    Vector3 startPos;

    [SerializeField] float speed = 3;
    [SerializeField] float maxForceHoldDownTime = 15f;
    [SerializeField] float maxJumpForce = 200f;

    [CurveRange(0, 0, 1, 1, EColor.Green)]
    public AnimationCurve acceleration;

    float accelerationAmount;

    float verticalMovement;
    Vector3 move;
    float holdDownStart;
    Rigidbody myRigidbody;
    SphereCollider myCollider;
    public bool touchingGround;
    public bool jumping;
    public bool running;
    public bool justLanded;
    Vector3 jump;
    Vector3 mousePos;
    public int counter;
    float jumpBonus;

    public int checkpointInt = 0;
    public int checkpointsPassed = 0;
    public int lastCheckpointInt = 0;

    [SerializeField] Text score;
    [SerializeField] Text scoreWin;
    public int highscore = 0;
    public bool gameOver;
    public bool gameWon;
    public bool restartGame;

    void Start()
    {
        startPos = transform.position;
        gameOver = false;
        gameWon = false;
        myRigidbody = GetComponent<Rigidbody>();
        myCollider = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, myCollider.radius, Vector3.down, out hit, 1f, ~LayerMask.GetMask("Player")))
        {
            touchingGround = true;

            //Debug.Log("WORKS");
            if(jumping == true && justLanded == false)
            {
                move.x = 0;
                jumping = false;
                justLanded = true;

               
                
            }
            //Debug.Log(touchingGround);
        }
        else
        {
            touchingGround = false;
            //Debug.Log(touchingGround);
        }

        if (justLanded == true) 
        { 
            running = true; 
        }

        verticalMovement = Input.GetAxis("Vertical");
        //move = new Vector3(1, 0, verticalMovement);

        if(jumping == false && gameOver == false && gameWon == false)
        {
            if (accelerationAmount < 1)
            {
                accelerationAmount += Time.deltaTime;
            }

            move = new Vector3(Mathf.Lerp(0, 1, acceleration.Evaluate(accelerationAmount)), 0, verticalMovement);

            //transform.Translate(move * Time.deltaTime * speed, Space.World);
            transform.Translate(move * Time.deltaTime * speed, Space.World);
            if (justLanded == true)
            {
                justLanded = false;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Down");
            //holdDownStart = Time.time;
        }

        if (Input.GetMouseButton(0))
        {
            //Counter 10
            holdDownStart += Time.deltaTime;

            if(counter < 12)
            {
                counter++;
            }
            else if(counter == 12)
            {
                counter = 0;
            }

            //Debug.Log(counter);
        }

        if (Input.GetMouseButtonUp(0) && touchingGround != false)
        {
            running = false;
            jumping = true;
            //Debug.Log("Up"); Debug.Log(holdDownStart);
            float holdDownTime = Time.time - holdDownStart;
            //StartCoroutine(Jump(CalculateHoldDownForce(holdDownTime)));
            mousePos = Input.mousePosition;
            mousePos = mousePos.normalized;
            switch(counter)
            {
                case 0:
                case 6:
                case 12:
                    jumpBonus = 0;
                    break;
                case 1:
                case 7:
                    jumpBonus = 0.2f;
                    break;
                case 2:
                case 8:
                    jumpBonus = 0.4f;
                    break;
                case 3:
                case 9:
                    jumpBonus = 0.6f;
                    break;
                case 4:
                case 10:
                    jumpBonus = 0.8f;
                    break;
                case 5:
                case 11:
                    jumpBonus = 1f;
                    highscore += 2;
                    score.text = highscore.ToString();
                    break;
            }
            //myRigidbody.AddForce(new Vector3(mousePos.x/2, mousePos.y * 2, 0) * (1f + CalculateHoldDownForce(holdDownStart)), ForceMode.Impulse);
            if(gameOver == false && gameWon == false)
            {
                float currentJumpForce = CalculateHoldDownForce(holdDownStart * (2 + jumpBonus));
                currentJumpForce = Mathf.Clamp(currentJumpForce, 0, 8);
                jump = new Vector3(move.x, mousePos.y * 5, 0) * (1f + currentJumpForce);
                Debug.Log(currentJumpForce);
            }

            myRigidbody.AddForce(jump, ForceMode.Impulse);
            holdDownStart = 0;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            running = false;
            jumping = true;
            if (gameOver == false && gameWon == false)
            {
                jump = new Vector3(move.x, 10, 0);
            }

            myRigidbody.AddForce(jump, ForceMode.Impulse);
        }

        if (restartGame)
        {
            transform.position = new Vector3(startPos.x, transform.position.y, transform.position.z);
            restartGame = false;
        }
    }

    private float CalculateHoldDownForce(float holdTime)
    {
        float calculateHoldForce = holdTime;
        float holdForceNormalized = Mathf.Clamp01(calculateHoldForce / maxForceHoldDownTime);
        float force = holdForceNormalized * maxJumpForce;
        //Debug.Log("Time: " + holdTime + " Force: " + force);
        return force;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Checkpoint1")
        {
            SoundToCollectPoints();
            checkpointInt = 1;
            CalculateCheckpoint(checkpointInt, lastCheckpointInt);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "Checkpoint2")
        {
            SoundToCollectPoints();
            checkpointInt = 2;
            CalculateCheckpoint(checkpointInt, lastCheckpointInt);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "Checkpoint3")
        {
            SoundToCollectPoints();
            checkpointInt = 3;
            CalculateCheckpoint(checkpointInt, lastCheckpointInt);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "Fly")
        {
            SoundToCollectPoints();
            highscore++;
            score.text = highscore.ToString();
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "GameOver")
        {
            gameOver = true;
        }
        else if (other.gameObject.tag == "GameWon")
        {
            gameWon = true;
            scoreWin.text = "Your score: " + highscore.ToString();
        }
        else if (other.gameObject.tag == "RestartGame")
        {
            restartGame = true;
        }

        Debug.Log(highscore);
    }

    /*private void OnTriggerExit(Collider other)
    {
        other.gameObject.SetActive(false);
    }*/

    void CalculateCheckpoint(int currentCheckpoint, int lastCheckpoint)
    {
        if(currentCheckpoint == lastCheckpoint)
        {
            if(checkpointsPassed == 0)
            {
                highscore += 3;
                score.text = highscore.ToString();
            }
            else
            {
                highscore += (3 + checkpointsPassed);
                score.text = highscore.ToString();
            }

            checkpointsPassed++;
            lastCheckpointInt = currentCheckpoint;
        }
        else
        {
            checkpointsPassed = 0;
            highscore += 3;
            score.text = highscore.ToString();
            lastCheckpointInt = currentCheckpoint;
            checkpointsPassed++;
        }
    }

    public void SoundToCollectPoints()
    {
        
       float rand = Random.Range(1, 3);

        if (rand == 1)
        {

            AudioManager.instance.PlaySound("Collect1");
        }
        else if (rand == 2)
        {

            AudioManager.instance.PlaySound("Collect2");
        }
        else if (rand == 3)
        {

            AudioManager.instance.PlaySound("Collect3");
        }

    }
}
