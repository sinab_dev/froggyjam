﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public UIManager uiMan;
    public CharacterController player;
    public GameObject score;
    public GameObject scoreWin;

    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player.gameOver)
        {
            uiMan.looseScreen.SetActive(true);
        }
        else if (player.gameWon)
        {
            uiMan.winScreen.SetActive(true);
            score.SetActive(false);
            scoreWin.SetActive(true);
        }
        else if (player.restartGame)
        {
            SceneManager.LoadScene("Blockout");
        }
    }
}
