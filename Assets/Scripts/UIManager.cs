﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    public GameObject winScreen;
    public GameObject looseScreen;

    

    public void Awake()
    {
        winScreen.SetActive(false);
        looseScreen.SetActive(false);
        //AudioManager.instance.PlaySound("BGSound");

    }
    public void Start()
    {
        

    }


    //BUTTONS

    public void QuitGame()
    {
        Application.Quit();

    }
    public void LoadGameScene()
    {
        SceneManager.LoadScene("Blockout");

    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenuu");

    }
    public void Credits()
    {
        SceneManager.LoadScene("Credits");

    }

    //UISCREENS



    // Sounds

    public void ButtonClick()
    {
        AudioManager.instance.PlaySound("Click");

    }

   

}
