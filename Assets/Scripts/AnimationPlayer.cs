﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour
{
    public CharacterController charCon;
    public Animator anim;
    



    public ParticleSystem partSyst;

    public int counter;


   
    public void Update()
    {
        partSyst.Stop();

        if (charCon.running == true)
        {
            anim.SetBool("IsWalking", true);
            anim.SetBool("IsJumping", false);
            anim.SetBool("IsLanding", false);
            anim.SetBool("TouchingGround", false);
            anim.SetBool("IsSuperJumping", false);
        }

        if (charCon.jumping == true)
        {
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsJumping", true);
            anim.SetBool("IsLanding", false);
            anim.SetBool("TouchingGround", false);
            anim.SetBool("IsSuperJumping", false);

            AudioManager.instance.PlaySound("Jump");


        }

        if (charCon.jumping == true && charCon.counter == 5 || charCon.jumping == true && charCon.counter == 12)
        {
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsJumping", false);
            anim.SetBool("IsLanding", false);
            anim.SetBool("TouchingGround", false);
            anim.SetBool("IsSuperJumping", true);

            AudioManager.instance.PlaySound("SuperJump");

            partSyst.Play();
            partSyst.Emit(4);

            //partSyst.loop = false;
            

        }
        if (charCon.touchingGround == true)
        {
            anim.SetBool("TouchingGround", true);

        }
        else
        {
            anim.SetBool("TouchingGround", false);

        }
        if (charCon.justLanded == true)
        {
            anim.SetBool("TouchingGround", true);
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsJumping", false);
            anim.SetBool("IsLanding", true);



        }

        if(charCon.gameWon == true)
        {
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsJumping", false);
            anim.SetBool("IsLanding", false);
            anim.SetBool("TouchingGround", false);
            anim.SetBool("IsSuperJumping", false);
            anim.SetBool("GameWon", true);
            anim.SetBool("GameLost", false);

        }
        if (charCon.gameOver == true)
        {
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsJumping", false);
            anim.SetBool("IsLanding", false);
            anim.SetBool("TouchingGround", false);
            anim.SetBool("IsSuperJumping", false);
            anim.SetBool("GameWon", false);
            anim.SetBool("GameLost", true);

        }

        /// Particle System
        ///

        /*if (Input.GetMouseButtonDown(0))
        {

            if (charCon.counter == 0 || charCon.counter == 6 || charCon.counter == 12)
            {
                zeroCirc.SetActive(true);
                oneCirc.SetActive(false);
                twoCirc.SetActive(false);
                threeCirc.SetActive(false);
                fourCirc.SetActive(false);
                fiveCirc.SetActive(false);
            }
            if (charCon.counter == 1 || charCon.counter == 7)
            {

                zeroCirc.SetActive(false);
                oneCirc.SetActive(true);
                twoCirc.SetActive(false);
                threeCirc.SetActive(false);
                fourCirc.SetActive(false);
                fiveCirc.SetActive(false);

            }
            if (charCon.counter == 2 || charCon.counter == 8)
            {
                zeroCirc.SetActive(false);
                oneCirc.SetActive(false);
                twoCirc.SetActive(true);
                threeCirc.SetActive(false);
                fourCirc.SetActive(false);
                fiveCirc.SetActive(false);
            }
            if (charCon.counter == 3 || charCon.counter == 9)
            {
                zeroCirc.SetActive(false);
                oneCirc.SetActive(false);
                twoCirc.SetActive(false);
                threeCirc.SetActive(true);
                fourCirc.SetActive(false);
                fiveCirc.SetActive(false);
            }
            if (charCon.counter == 4 || charCon.counter == 10)
            {
                zeroCirc.SetActive(false);
                oneCirc.SetActive(false);
                twoCirc.SetActive(false);
                threeCirc.SetActive(false);
                fourCirc.SetActive(true);
                fiveCirc.SetActive(false);
            }
            if (charCon.counter == 5 || charCon.counter == 11)
            {
                zeroCirc.SetActive(false);
                oneCirc.SetActive(false);
                twoCirc.SetActive(false);
                threeCirc.SetActive(false);
                fourCirc.SetActive(false);
                fiveCirc.SetActive(true);
            }


        }*/




    }
}
