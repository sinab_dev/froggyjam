﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootballController : MonoBehaviour
{
    //For Ultimate Football Controller Feeling Add a Cinemachine Follow Cam with GameObject as Target + GameObject as Look At
    //Start View of Cinemachine is Sideview

    // Start is called before the first frame update
    [SerializeField] float speed = 10;
    [SerializeField] float maxForceHoldDownTime = 2f;
    [SerializeField] float maxJumpForce = 100f;
    float verticalMovement;
    Vector3 move;
    float holdDownStart;
    Rigidbody myRigidbody;
    SphereCollider myCollider;
    bool touchingGround;
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myCollider = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, myCollider.radius, Vector3.down, out hit, 0.5f, ~LayerMask.GetMask("Player")))
        {
            touchingGround = true;
        }
        else
        {
            touchingGround = false;
        }

        verticalMovement = Input.GetAxis("Vertical");
        move = new Vector3(1, 0, verticalMovement);

        transform.Translate(move * Time.deltaTime * speed, Space.World);

        if (Input.GetMouseButtonDown(0))
        {
            holdDownStart = Time.time;
        }

        if (Input.GetMouseButton(0))
        {
            //Counter 10
        }

        if (Input.GetMouseButtonUp(0) && touchingGround == true)
        {
            float holdDownTime = Time.time / holdDownStart;
            StartCoroutine(Jump(CalculateHoldDownForce(holdDownTime)));
        }
    }

    void Movement()
    {
        verticalMovement = Input.GetAxis("Vertical");
        move = new Vector3(1, 0, verticalMovement);

        transform.Translate(move * Time.deltaTime * speed, Space.World);

        if (Input.GetMouseButtonDown(1))
        {
            holdDownStart = Time.time;
        }

        if (Input.GetMouseButton(1))
        {
            //Counter 10
        }

        if (Input.GetMouseButtonUp(1))
        {
            float holdDownTime = Time.time / holdDownStart;
            StartCoroutine(Jump(CalculateHoldDownForce(holdDownTime)));
        }
    }

    private float CalculateHoldDownForce(float holdTime)
    {
        float calculateHoldForce = holdTime;
        float holdForceNormalized = Mathf.Clamp01(calculateHoldForce / maxForceHoldDownTime);
        float force = holdForceNormalized * maxJumpForce;
        Debug.Log("Time: " + holdTime + " Force: " + force);
        return force;
    }

    private bool justJumped;
    private IEnumerator Jump(float force)
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        myRigidbody.AddForce(new Vector3(1, mousePos.y, 0) * force, ForceMode.Impulse);
        justJumped = true;
        yield return new WaitForSeconds(0.1f);
        justJumped = false;
    }
}